# Dan's Neovim Config

## External dependencies

### prettierd
Daemonized prettier, improves formatting speed.
https://github.com/fsouza/prettierd

### fzf
Fuzzy finder. Used by Telescope.
https://github.com/junegunn/fzf
